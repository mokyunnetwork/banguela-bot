FROM node:10-alpine

MAINTAINER Natan <natan@mokyun.net>

# Set config ENVs and ARGs. You can define here or use docker-compose to do that (recomended!!).
ARG APP_ENV
ARG PORT=5000

##
# Application
##

# Set Application dir
WORKDIR /app

# Copy files
COPY . .

# Get configuration
RUN echo ${APP_ENV} | base64 -d > .env

# Install dependencies
RUN \
    apk update \
    && apk add --no-cache --virtual .build-deps \
        git \
    && npm install

# Cleanup
RUN \
    apk del .build-deps

# Run
EXPOSE ${PORT}
ENTRYPOINT npm start
