const { Command, util: { exec, codeBlock } } = require('klasa');

module.exports = class extends Command {

  constructor(...args) {
    super(...args, {
      description: 'Desliga o bot.',
      guarded: true,
      permissionLevel: 10
    });
  }

  async run(msg) {
    await msg.react('✅');
    await process.exit().catch(error => msg.send(error));
  }

};