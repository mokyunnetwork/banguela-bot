const { Command } = require('klasa');

module.exports = class extends Command {

  constructor(...args) {
    super(...args, {
      runIn: ["text"],
      cooldown: 10,
      guarded: true,
      hidden: true,
      permissionLevel: 2,
      description: 'Altera o modo de spoiler no canal.',
      usage: "[canal:channelname]",
    });
  }

  async run(msg, [canal = msg.channel]) {
    if (msg.guild.settings.get('spoilerChannel').indexOf(canal.id) === -1) {
      await msg.guild.settings.update("spoilerChannel", canal, { action: "add" });
      return msg.send(`Modo spoiler do canal \`#${canal.name}\` foi **habilitado**`);
    } else {
      await msg.guild.settings.update("spoilerChannel", canal, { action: "remove" });
      return msg.send(`Modo spoiler do canal \`#${canal.name}\` foi **desabilitado**`);
    }
  }

};
