require('dotenv').config()

const { Client } = require('klasa')
const { config, token } = require('./config')

Client.use(require('klasa-dashboard-hooks'))

Client.defaultPermissionLevels
  .add(2, ({ guild, member }) => guild && member.permissions.has('MANAGE_MESSAGES'), { fetch: true })
  .add(3, ({ guild, member }) => guild && member.permissions.has('MANAGE_ROLES'), { fetch: true })
  .add(4, ({ guild, member }) => guild && member.permissions.has('KICK_MEMBERS'), { fetch: true })
  .add(5, ({ guild, member }) => guild && member.permissions.has('BAN_MEMBERS'), { fetch: true })

Client.defaultGuildSchema
  .add('spoilerChannel', 'channel', {
    default: [],
    array: true
  })

class Banguela extends Client {
  constructor(...args) {
    super(...args)
  }
}

new Banguela(config).login(token)
