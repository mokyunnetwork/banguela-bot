const { Monitor } = require("klasa");

module.exports = class extends Monitor {

  constructor(...args) {
    super(...args, {
      ignoreBlacklistedUsers: true,
      ignoreBlacklistedGuilds: true,
      ignoreBots: true,
      ignoreOthers: false,
      ignoreEdits: false,
      ignoreSelf: true
    });
  }

  async run(msg) {
    if (!msg.guild || msg.command) return;
    if (msg.member.hasPermission('MANAGE_MESSAGES')) return;
    if (msg.guild.settings.get('spoilerChannel').indexOf(msg.channel.id) === -1) return;
    if (msg.attachments.first() && !msg.attachments.first().name.startsWith('SPOILER_')) return msg.delete().catch(() => null);
    if (msg.content && /^(http(s?):)\S*/g.test(msg.content) && !msg.content.startsWith('||') && !msg.content.endsWith('||')) return msg.delete().catch(() => null);
  }
}