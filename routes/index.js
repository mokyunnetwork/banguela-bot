const { Route } = require('klasa-dashboard-hooks');
const { Duration } = require('klasa');

module.exports = class extends Route {

	constructor(...args) {
		super(...args, { route: '' });
	}

	get(request, response) {
		return response.end('OK!');
	}

};
